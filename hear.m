clear all; close all;

% -------------------------------------------------------------------------
%   This module contains the code used to conduct the hearing test.
% -------------------------------------------------------------------------

% Test the function that we are given.
base_unit = 0.00005;
A = base_unit;    % Amplitude
f_Hz = 440; % Frequency
d_sec = 1;  % Duration (in seconds)
fs = 44100; % Sampling rate 44.1 kHz

freqs = linspace(100, 20000, 10);
amps = zeros(1, length(freqs));

% At each frequency we want to start low and bring the sound up until it
% can be heard.
for i = 1:length(freqs)
    can_hear = false;
    amp_1 = 0;
    while can_hear == false
        can_hear = hear_sinus(A, f_Hz, d_sec, fs);
        if (can_hear == false)
            A = A + base_unit;
        end
        amp_1 = A;
    end

    amp_2 = 0;
    A = amp_1 + (5 * base_unit);
    while can_hear == true
        can_hear = hear_sinus(A, f_Hz, d_sec, fs);
        if (can_hear == true)
            A = A - base_unit;
        end
        amp_2 = A;
    end

    ave_amp = abs(amp_1 - amp_2) / 2
    if (amp_1 > amp_2)
        ave_amp = ave_amp + amp_2;
    else
        ave_amp = ave_amp + amp_1;
    end
    
    amps(i) = ave_amp;
end

% Write the results to a file.
fid = fopen('hearing_test.csv', 'w');
for i = 1:length(freqs)
    fprintf(fid, '%.4f, %.4f\n', freqs(i), amps(i)); 
end
fclose(fid);



