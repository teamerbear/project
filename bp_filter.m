function [passband_limits, stopband_limits] = bp_filter(L, passband_level, stopband_level, absolute)
%BP_FILTER Plots the stop/pass bands for the given filter length.
%   Parameters:
%       L - the filter length
%       passband_level - passband amplitude must be greater than this
%       stopband_level - stopband amplitude must be less than this
%       absolute - a flag indicating whether or not the passband and
%       stopband levels are absolute or fractions of the max amplitude.

% Measure the gain of this filter at the three freqs of interest:
%   0.3pi, 0.44pi, and 0.7pi
l = linspace(0, L-1, L);

% Generate the filter coefficients.
bb = 0.2 .* cos(0.44.*pi.*l);

% See the frequency response for the filter.
ww = -pi:(pi/100):pi;
bp = freqz(bb, 1, ww);
f = figure;
subplot(2,1,1);
grid on;
plot(ww, abs(bp));
yhi = max(abs(bp));
yfrac = yhi-floor(yhi);
if (yfrac >= 0.5)
    yhi = ceil(yhi);
else
    if (yfrac < 0.5)
        yhi = floor(yhi) + 0.5;
    end
end
ylimits = [0 yhi];
ylim(ylimits);
xlim([-pi pi]);
hold on;
plot([0.3.*pi 0.3.*pi], ylimits, '--r');
plot([0.44.*pi 0.44.*pi], ylimits, '--r');
plot([0.7.*pi 0.7.*pi], ylimits, '--r');
plot([-0.3.*pi -0.3.*pi], ylimits, '--r');
plot([-0.44.*pi -0.44.*pi], ylimits, '--r');
plot([-0.7.*pi -0.7.*pi], ylimits, '--r');
plot(xlim(), [0.5 0.5], '--k');
plot(xlim(), [2.2 2.2], '--k');
title(strcat('Simple Bandpass Freq Response (L = ', num2str(L), ')'))
subplot(2,1,2);
plot(ww, angle(bp))
title(strcat('Simple Bandpass Phase (L = ', num2str(L), ')'))
xlabel('Normalized Radian Frequency')
saveas(f, strcat('bp_freqz_', num2str(L), '.png'))

passband = [];
stopband = [];
if absolute
    passband = find(abs(bp) >= (passband_level));
    stopband = find(abs(bp) <= (stopband_level));
else
    passband = find(abs(bp) >= (passband_level .* max(abs(bp))));
    stopband = find(abs(bp) <= (stopband_level .* max(abs(bp))));
end

% Find the actual limits of the passband(s).
passband_limits = [];
curr_low = ww(passband(1));
curr_high = 0;
for i = 1:(length(passband) - 1)
   if passband(i+1) ~= (passband(i) + 1)
        curr_high = ww(passband(i));
        passband_limits = cat(1, passband_limits, [curr_low curr_high]);
        curr_low = ww(passband(i+1));
   end
   if i == (length(passband) - 1)
        curr_high = ww(passband(i+1));
        passband_limits = cat(1, passband_limits, [curr_low curr_high]);
   end
end

% Find the actual limits of the stop band (from the plot above we see that
% there are two stop bands).
stopband_limits = [];
curr_low = ww(stopband(1));
curr_high = 0;
for i = 1:(length(stopband) - 1)
    if stopband(i+1) ~= (stopband(i) + 1)
        curr_high = ww(stopband(i));
        stopband_limits = cat(1, stopband_limits, [curr_low curr_high]);
        curr_low = ww(stopband(i+1));
    end
    if i == (length(stopband) - 1)
        curr_high = ww(stopband(i+1));
        stopband_limits = cat(1, stopband_limits, [curr_low curr_high]);
   end
end

% Draw a nice plot to visualize the passbands and stop bands.
fvis = figure;
plot(ww, abs(bp));
ylim(ylimits);
xlabel('Normalized Radian Frequency');
ylabel('Relative Amplitude');
title(strcat('Passband/Stopband Visualization (L = ', num2str(L), ')'));
hold on;

% Loop through the band limits and plot them.
dims = size(passband_limits);
for i = 1:dims(1)
    plot([passband_limits(i,1) passband_limits(i,1)], [ylimits(1) ylimits(2)], 'k--');
    plot([passband_limits(i,2) passband_limits(i,2)], [ylimits(1) ylimits(2)], 'k--');
    rectangle('Position', ...
        [passband_limits(i,1) ylimits(1) (passband_limits(i,2) - passband_limits(i,1)) diff(ylimits)], ...
        'FaceColor', [0.5 1 0.5 0.3], 'EdgeColor', 'none');
end

dims = size(stopband_limits);
for i = 1:dims(1)
    plot([stopband_limits(i,1) stopband_limits(i,1)], [ylimits(1) ylimits(2)], 'k--');
    plot([stopband_limits(i,2) stopband_limits(i,2)], [ylimits(1) ylimits(2)], 'k--');
    rectangle('Position', ...
        [stopband_limits(i,1) ylimits(1) (stopband_limits(i,2) - stopband_limits(i,1)) diff(ylimits)], ...
        'FaceColor', [1 0.5 0.5 0.3], 'EdgeColor', 'none');
end

pbaspect([2 1 1]);
saveas(fvis, strcat('band_vis_', num2str(L), '.png'))

end

