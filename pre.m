clear all; close all;

% Run the example given in the lab about the two-point averaging system.
bb = [0.5 0.5]; % filter coefficients
ww = -pi:pi/100:pi; % frequency vector
H = freqz(bb, 1, ww);
subplot(2,1,1);
plot(ww, abs(H)), grid on
subplot(2,1,2);
plot(ww, angle(H)), grid on
xlabel('Normalized Radian Frequency')

% -------------------------------------------------------------------------
% Frequency Response of Four Point Averager
% -------------------------------------------------------------------------
what = -pi:(2*pi/400):(pi-(2*pi/400));
C = (2 .* cos(0.5 .* what) + 2 .* cos(1.5 .* what)) ./ 4;
psi = -1.5 .* what;
f = figure;
subplot(2,1,1);
plot(what, C); grid on
title('$C(\hat{\omega})$', 'Interpreter', 'latex')
subplot(2,1,2);
plot(what, psi); grid on
title('$\psi(\hat{\omega})$', 'Interpreter', 'latex')

bb = 1/4 * ones(1,4);
H = freqz(bb, 1, what);
f = figure;
subplot(2,1,1);
plot(what, abs(H)); grid on
title('$\vert H(\hat{\omega})\vert$', 'Interpreter', 'latex')
subplot(2,1,2);
plot(what, angle(H)); grid on
title('$\angle H(\hat{\omega})$', 'Interpreter', 'latex')
hold on
plot(what, pi .* ones(length(what)));
plot(what, -pi .* ones(length(what)));

% -------------------------------------------------------------------------
% The MATLAB find Function
% -------------------------------------------------------------------------

ww = -pi:(pi/500):(pi - (pi/500));
HH = freqz(bb, 1, ww);
zero_inds = find(abs(HH) < 1e-8)
zero_freqs = ww(zero_inds)



