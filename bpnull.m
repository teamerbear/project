clear all; close all;

% -------------------------------------------------------------------------
%   2.2 Cascading Two Systems
% -------------------------------------------------------------------------

alpha = 0.8;
M = 9;

l = linspace(0,9,10);

% Generate the filter coefficients for Filter 1
bb = alpha .^ l;

% See the frequency response for the filter.
 ww = -pi:(pi/100):pi;
HH_1 = freqz(bb, 1, ww);
subplot(2,1,1);
plot(ww, abs(HH_1))
title('Filter 1 Freq Response')
subplot(2,1,2);
plot(ww, angle(HH_1))
title('Filter 1 Phase')
xlabel('Normalized Radian Frequency')
print('fir_1_freqz.png', '-dpng')

% Generate the filter coefficients for Filter 2
bb = [1 -alpha];

% See the frequency response for the filter.
figure;
HH_2 = freqz(bb, 1, ww);
subplot(2,1,1);
plot(ww, abs(HH_2))
title('Filter 2 Freq Response')
subplot(2,1,2);
plot(ww, angle(HH_2))
title('Filter 2 Phase')
xlabel('Normalized Radian Frequency')
print('fir_2_freqz.png', '-dpng')

% Generate the filter coefficients for the cascaded filters.
bb = linspace(0,M+1,M+2);
bb = alpha .^ bb;
bb(M+2) = 0;
l = linspace(1,M+1,M+1);
l = alpha .^ l;
l = alpha .* l;
bb(2:M+2) = bb(2:M+2) - l;

% Plot the frequency and phase response for the cascaded filters.
figure;
HH_3 = freqz(bb, 1, ww);
subplot(2,1,1);
plot(ww, abs(HH_3))
title('Cascaded Filter Freq Response')
subplot(2,1,2);
plot(ww, angle(HH_3))
title('Cascaded Filter Phase')
xlabel('Normalized Radian Frequency')
print('fir_casc_freqz.png', '-dpng')

% -------------------------------------------------------------------------
%   3.1 Nulling Filters for Rejection
% -------------------------------------------------------------------------

% Create an input signal.
n = 0:149;
x_in = 5 .* cos(0.3.*pi.*n) + 22 .* cos(0.44.*pi.*n - (pi./3)) ...
        + 22 .* cos(0.7.*pi.*n - (pi./4));

% Coefficients for the two simple FIR filters.
b_fir_1 = [1 -2.*cos(0.44.*pi) 1];
b_fir_2 = [1 -2.*cos(0.7.*pi) 1];

% Now implement the cascading filters. (Note that each convolution adds an
% extra two points.)
y = conv(x_in, b_fir_1);
y = conv(y, b_fir_2);

% Plot the input versus the output.
figure;
subplot(2,1,1);
plot(n, x_in);
title('Input Signal x(n)');
subplot(2,1,2);
plot(n(1:40), y(1:40));
xlabel('n');
title('Filtered Output y(n)');
print('casc_filter_output.png', '-dpng');

% Observe the output signal and generate the signal by hand. Compare the
% two.
by_hand = 9 .* cos(0.3.*pi.*(n-2));
err = abs(by_hand - y(1:150));
figure;
subplot(2,1,1);
plot(n(1:40), y(1:40));
hold on;
plot(n(1:40), by_hand(1:40));
ylabel('y(n)');
title('Output Comparison');
subplot(2,1,2);
plot(n(1:40), err(1:40));
title('Absolute Error');
xlabel('n');
ylabel('\mid y(1)-y(2) \mid');
print('output_comparison.png', '-dpng');

% -------------------------------------------------------------------------
%   3.2 Simple Bandpass Filter Design
% -------------------------------------------------------------------------

% Make plots of filters with length 10, 20, and 40.
[bp_10, sb_10] = bp_filter(10, 0.707, 0.25, false);
bp_10 ./ pi
bp_filter(20, 0.707, 0.25, false);
bp_filter(40, 0.707, 0.25, false);

% Comment on the selectivity of the L = 10 bandpass filter. In other words,
% which frequencies are passed by the filter? Use the frequency response to
% explain how the filter can pass one component at 0.44pi while reducing or
% rejecting the others at 0.3pi and 0.7pi.
%
% The L=10 filter passes frequencies from 0.37pi to 0.52 pi.  

% Generate a bandpass filter that will pass the frequency component at
% 0.44pi, but now make the filter length (L) long enough so that it will
% also greatly reduce frequency components at (or near) 0.3pi and 0.7pi.
% Determine the smallest value of L so that
%   - Any frequency component satisfying abs(w) <= 0.3pi will be reduced by
%     a factor of 10 or more.
%   - Any frequency component satisfying 0.7pi <= abs(w) <= pi will be
%     reduced by a factor of 10 or more.
%
% We want a filter that makes the lower output lower than 0.5 and the
% higher frequency outputs lower than 2.2. We can see from the plot of
% L=10, that it acheives these goals. Anything below 10 results in a
% passband amplitude that is less than one (the assignment is unclear as to
% what is "passing" for this filter).

% Use the filter from the previous part to filter the sum of 3 sinusoids
% signal from section 3.1.  Make a plot of 100 points of the input and
% output signals and explain how the filter has reduced or removed two of
% the three sinusoidal components.

% Generate the filter coefficients.
L = 10;
l = linspace(0, L-1, L);
fir_coeffs = 0.2 .* cos(0.44.*pi.*l);
out = conv(x_in, fir_coeffs, 'same');
f2 = figure;
subplot(2,1,1);
plot(0:99, x_in(1:100))
title('Input Signal x(n)')
subplot(2,1,2);
plot(0:99, out(1:100))
title('Bandpass Convolution y(n)')
saveas(f2, 'bandpass_conv.png')



