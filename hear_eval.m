clear all; close all;

data = csvread('hearing_test.csv');
freqs = data(:,1);
amps = data(:,2);
amps = amps/max(amps);
plot(freqs, amps);
xlabel('Frequency (Hz)');
ylabel('Relative Amplitude Required');
title('Hearing Evaluation');
print('hear_eval.png', '-dpng');
